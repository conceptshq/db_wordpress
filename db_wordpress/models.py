# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class WpEmailsTracking(models.Model):
    id = models.BigAutoField(db_column='ID', unique=True,primary_key=True)  # Field name made lowercase.
    hash = models.TextField()
    send_to = models.CharField(max_length=255)
    time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp__emails_tracking'


class WpCimyUefData(models.Model):
    id = models.BigAutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    user_id = models.BigIntegerField(db_column='USER_ID')  # Field name made lowercase.
    field_id = models.BigIntegerField(db_column='FIELD_ID')  # Field name made lowercase.
    value = models.TextField(db_column='VALUE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'wp_cimy_uef_data'


class WpCimyUefFields(models.Model):
    id = models.BigAutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    f_order = models.BigIntegerField(db_column='F_ORDER')  # Field name made lowercase.
    fieldset = models.BigIntegerField(db_column='FIELDSET')  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=20, blank=True, null=True)  # Field name made lowercase.
    label = models.TextField(db_column='LABEL', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='DESCRIPTION', blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(db_column='TYPE', max_length=20, blank=True, null=True)  # Field name made lowercase.
    rules = models.TextField(db_column='RULES', blank=True, null=True)  # Field name made lowercase.
    value = models.TextField(db_column='VALUE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'wp_cimy_uef_fields'


class WpCimyUefWpFields(models.Model):
    id = models.BigAutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    f_order = models.BigIntegerField(db_column='F_ORDER')  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=20, blank=True, null=True)  # Field name made lowercase.
    label = models.TextField(db_column='LABEL', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='DESCRIPTION', blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(db_column='TYPE', max_length=20, blank=True, null=True)  # Field name made lowercase.
    rules = models.TextField(db_column='RULES', blank=True, null=True)  # Field name made lowercase.
    value = models.TextField(db_column='VALUE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'wp_cimy_uef_wp_fields'


class WpCommentmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    comment_id = models.BigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_commentmeta'


class WpComments(models.Model):
    comment_id = models.BigAutoField(db_column='comment_ID', primary_key=True)  # Field name made lowercase.
    comment_post_id = models.BigIntegerField(db_column='comment_post_ID')  # Field name made lowercase.
    comment_author = models.TextField()
    comment_author_email = models.CharField(max_length=100)
    comment_author_url = models.CharField(max_length=200)
    comment_author_ip = models.CharField(db_column='comment_author_IP', max_length=100)  # Field name made lowercase.
    comment_date = models.DateTimeField()
    comment_date_gmt = models.DateTimeField()
    comment_content = models.TextField()
    comment_karma = models.IntegerField()
    comment_approved = models.CharField(max_length=20)
    comment_agent = models.CharField(max_length=255)
    comment_type = models.CharField(max_length=20)
    comment_parent = models.BigIntegerField()
    user_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_comments'


class WpCspAccess(models.Model):
    access = models.CharField(max_length=255, blank=True, null=True)
    type_access = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_csp_access'


class WpCspEmailTemplates(models.Model):
    label = models.CharField(max_length=128)
    subject = models.CharField(max_length=255)
    body = models.TextField()
    variables = models.TextField()
    active = models.IntegerField()
    name = models.CharField(unique=True, max_length=128)
    module = models.CharField(max_length=128)

    class Meta:
        managed = False
        db_table = 'wp_csp_email_templates'


class WpCspFiles(models.Model):
    pid = models.IntegerField()
    name = models.CharField(max_length=255)
    path = models.CharField(max_length=255)
    mime_type = models.CharField(max_length=255, blank=True, null=True)
    size = models.IntegerField()
    active = models.IntegerField()
    date = models.DateTimeField(blank=True, null=True)
    download_limit = models.IntegerField()
    period_limit = models.IntegerField()
    description = models.TextField()
    type_id = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_csp_files'


class WpCspHtmltype(models.Model):
    label = models.CharField(unique=True, max_length=32)
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'wp_csp_htmltype'


class WpCspLog(models.Model):
    type = models.CharField(max_length=64)
    data = models.TextField(blank=True, null=True)
    date_created = models.IntegerField()
    uid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_csp_log'


class WpCspModules(models.Model):
    code = models.CharField(unique=True, max_length=64)
    active = models.IntegerField()
    type_id = models.SmallIntegerField()
    params = models.TextField(blank=True, null=True)
    has_tab = models.IntegerField()
    label = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    ex_plug_dir = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_csp_modules'


class WpCspModulesType(models.Model):
    label = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'wp_csp_modules_type'


class WpCspOptions(models.Model):
    code = models.CharField(unique=True, max_length=64)
    value = models.TextField(blank=True, null=True)
    label = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    htmltype_id = models.SmallIntegerField()
    params = models.TextField(blank=True, null=True)
    cat_id = models.IntegerField(blank=True, null=True)
    sort_order = models.IntegerField(blank=True, null=True)
    value_type = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_csp_options'


class WpCspOptionsCategories(models.Model):
    label = models.CharField(max_length=128)

    class Meta:
        managed = False
        db_table = 'wp_csp_options_categories'


class WpCspSubscribers(models.Model):
    user_id = models.IntegerField()
    email = models.CharField(max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)
    created = models.DateTimeField()
    unsubscribe_date = models.DateTimeField(blank=True, null=True)
    active = models.IntegerField()
    token = models.CharField(max_length=255, blank=True, null=True)
    ip = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_csp_subscribers'


class WpDuplicatorPackages(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=250)
    hash = models.CharField(max_length=50)
    status = models.IntegerField()
    created = models.DateTimeField()
    owner = models.CharField(max_length=60)
    package = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_duplicator_packages'


class WpFollowupCouponLogs(models.Model):
    id = models.BigAutoField(primary_key=True)
    coupon_id = models.BigIntegerField()
    coupon_name = models.CharField(max_length=100)
    email_name = models.CharField(max_length=100)
    email_address = models.CharField(max_length=255)
    coupon_code = models.CharField(max_length=100)
    coupon_used = models.IntegerField()
    date_sent = models.DateTimeField()
    date_used = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_followup_coupon_logs'


class WpFollowupCoupons(models.Model):
    id = models.BigAutoField(primary_key=True)
    coupon_name = models.CharField(max_length=100)
    coupon_type = models.CharField(max_length=25)
    coupon_prefix = models.CharField(max_length=25)
    amount = models.FloatField()
    individual = models.IntegerField()
    exclude_sale_items = models.IntegerField()
    before_tax = models.IntegerField()
    free_shipping = models.IntegerField()
    usage_count = models.BigIntegerField()
    expiry_value = models.CharField(max_length=3)
    expiry_type = models.CharField(max_length=25)
    product_ids = models.CharField(max_length=255)
    exclude_product_ids = models.CharField(max_length=255)
    product_categories = models.TextField(blank=True, null=True)
    exclude_product_categories = models.TextField(blank=True, null=True)
    minimum_amount = models.CharField(max_length=50)
    maximum_amount = models.CharField(max_length=50)
    usage_limit = models.CharField(max_length=3)
    usage_limit_per_user = models.CharField(max_length=3)

    class Meta:
        managed = False
        db_table = 'wp_followup_coupons'


class WpFollowupCustomerCarts(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.BigIntegerField()
    user_email = models.CharField(max_length=100)
    cart_items = models.TextField()
    cart_total = models.FloatField()
    date_updated = models.DateTimeField()
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'wp_followup_customer_carts'


class WpFollowupCustomerNotes(models.Model):
    id = models.BigAutoField(primary_key=True)
    followup_customer_id = models.BigIntegerField()
    author_id = models.BigIntegerField()
    note = models.TextField(blank=True, null=True)
    date_added = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_followup_customer_notes'


class WpFollowupCustomerOrders(models.Model):
    followup_customer_id = models.BigIntegerField()
    order_id = models.BigIntegerField()
    price = models.FloatField()

    class Meta:
        managed = False
        db_table = 'wp_followup_customer_orders'


class WpFollowupCustomers(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.BigIntegerField()
    email_address = models.CharField(max_length=255)
    total_purchase_price = models.FloatField()
    total_orders = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_followup_customers'


class WpFollowupEmailCoupons(models.Model):
    email_id = models.BigIntegerField()
    send_coupon = models.IntegerField()
    coupon_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_followup_email_coupons'


class WpFollowupEmailExcludes(models.Model):
    id = models.BigAutoField(primary_key=True)
    email_id = models.BigIntegerField()
    order_id = models.BigIntegerField()
    email_name = models.CharField(max_length=255)
    email = models.CharField(max_length=100)
    date_added = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_followup_email_excludes'


class WpFollowupEmailLogs(models.Model):
    id = models.BigAutoField(primary_key=True)
    email_order_id = models.BigIntegerField()
    email_id = models.BigIntegerField()
    user_id = models.BigIntegerField()
    email_name = models.CharField(max_length=100)
    customer_name = models.CharField(max_length=255)
    email_address = models.CharField(max_length=255)
    date_sent = models.DateTimeField()
    order_id = models.BigIntegerField()
    product_id = models.BigIntegerField()
    email_trigger = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'wp_followup_email_logs'


class WpFollowupEmailOrderCoupons(models.Model):
    email_order_id = models.BigIntegerField()
    coupon_name = models.CharField(max_length=100)
    coupon_code = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'wp_followup_email_order_coupons'


class WpFollowupEmailOrders(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.BigIntegerField()
    user_email = models.CharField(max_length=255)
    order_id = models.BigIntegerField()
    product_id = models.BigIntegerField()
    email_id = models.CharField(max_length=100)
    send_on = models.BigIntegerField()
    is_cart = models.IntegerField()
    is_sent = models.IntegerField()
    date_sent = models.DateTimeField()
    email_trigger = models.CharField(max_length=100)
    meta = models.TextField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_followup_email_orders'


class WpFollowupEmailTracking(models.Model):
    id = models.BigAutoField(primary_key=True)
    event_type = models.CharField(max_length=20)
    email_order_id = models.BigIntegerField()
    email_id = models.BigIntegerField()
    user_id = models.BigIntegerField()
    user_email = models.CharField(max_length=255)
    target_url = models.CharField(max_length=255)
    client_name = models.CharField(max_length=100)
    client_version = models.CharField(max_length=25)
    client_type = models.CharField(max_length=50)
    user_ip = models.CharField(max_length=100)
    user_country = models.CharField(max_length=100)
    date_added = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_followup_email_tracking'


class WpFollowupFollowupHistory(models.Model):
    id = models.BigAutoField(primary_key=True)
    followup_id = models.BigIntegerField()
    user_id = models.BigIntegerField()
    content = models.TextField()
    date_added = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_followup_followup_history'


class WpFollowupOrderCategories(models.Model):
    order_id = models.BigIntegerField()
    category_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_followup_order_categories'


class WpFollowupOrderItems(models.Model):
    order_id = models.BigIntegerField()
    product_id = models.BigIntegerField()
    variation_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_followup_order_items'


class WpFollowupSubscriberLists(models.Model):
    id = models.BigAutoField(primary_key=True)
    list_name = models.CharField(max_length=100)
    access = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_followup_subscriber_lists'


class WpFollowupSubscribers(models.Model):
    id = models.BigAutoField(primary_key=True)
    email = models.CharField(max_length=100)
    date_added = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_followup_subscribers'


class WpFollowupSubscribersToLists(models.Model):
    subscriber_id = models.BigIntegerField()
    list_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_followup_subscribers_to_lists'


class WpLayerslider(models.Model):
    author = models.IntegerField()
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)
    data = models.TextField()
    date_c = models.IntegerField()
    date_m = models.IntegerField()
    flag_hidden = models.IntegerField()
    flag_deleted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_layerslider'


class WpLinks(models.Model):
    link_id = models.BigAutoField(primary_key=True)
    link_url = models.CharField(max_length=255)
    link_name = models.CharField(max_length=255)
    link_image = models.CharField(max_length=255)
    link_target = models.CharField(max_length=25)
    link_description = models.CharField(max_length=255)
    link_visible = models.CharField(max_length=20)
    link_owner = models.BigIntegerField()
    link_rating = models.IntegerField()
    link_updated = models.DateTimeField()
    link_rel = models.CharField(max_length=255)
    link_notes = models.TextField()
    link_rss = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'wp_links'


class WpNfObjectmeta(models.Model):
    id = models.BigAutoField(primary_key=True)
    object_id = models.BigIntegerField()
    meta_key = models.CharField(max_length=255)
    meta_value = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_nf_objectmeta'


class WpNfObjects(models.Model):
    id = models.BigAutoField(primary_key=True)
    type = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'wp_nf_objects'


class WpNfRelationships(models.Model):
    id = models.BigAutoField(primary_key=True)
    child_id = models.BigIntegerField()
    parent_id = models.BigIntegerField()
    child_type = models.CharField(max_length=255)
    parent_type = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'wp_nf_relationships'


class WpNinjaForms(models.Model):
    data = models.TextField()
    date_updated = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_ninja_forms'


class WpNinjaFormsFavFields(models.Model):
    row_type = models.IntegerField()
    type = models.CharField(max_length=255)
    order = models.IntegerField()
    data = models.TextField()
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'wp_ninja_forms_fav_fields'


class WpNinjaFormsFields(models.Model):
    form_id = models.IntegerField()
    type = models.CharField(max_length=255)
    order = models.IntegerField()
    data = models.TextField()
    fav_id = models.IntegerField(blank=True, null=True)
    def_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_ninja_forms_fields'


class WpNinjaFormsSubs(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    form_id = models.IntegerField()
    status = models.IntegerField()
    action = models.CharField(max_length=255)
    data = models.TextField()
    date_updated = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_ninja_forms_subs'


class WpOmLeads(models.Model):
    lead_id = models.BigAutoField(primary_key=True)
    optin_id = models.IntegerField()
    lead_name = models.CharField(max_length=128)
    lead_email = models.CharField(unique=True, max_length=128)
    lead_type = models.CharField(max_length=128)
    user_agent = models.CharField(max_length=128)
    referrer = models.CharField(max_length=256)
    referred_from = models.CharField(max_length=256)
    post_id = models.IntegerField()
    date_added = models.DateTimeField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_om_leads'


class WpOptions(models.Model):
    option_id = models.BigAutoField(primary_key=True)
    option_name = models.CharField(unique=True, max_length=191, blank=True, null=True)
    option_value = models.TextField()
    autoload = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'wp_options'


class WpPostmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    post_id = models.BigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_postmeta'


class WpPosts(models.Model):
    id = models.BigAutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    post_author = models.BigIntegerField()
    post_date = models.DateTimeField()
    post_date_gmt = models.DateTimeField()
    post_content = models.TextField()
    post_title = models.TextField()
    post_excerpt = models.TextField()
    post_status = models.CharField(max_length=20)
    comment_status = models.CharField(max_length=20)
    ping_status = models.CharField(max_length=20)
    post_password = models.CharField(max_length=20)
    post_name = models.CharField(max_length=200)
    to_ping = models.TextField()
    pinged = models.TextField()
    post_modified = models.DateTimeField()
    post_modified_gmt = models.DateTimeField()
    post_content_filtered = models.TextField()
    post_parent = models.BigIntegerField()
    guid = models.CharField(max_length=255)
    menu_order = models.IntegerField()
    post_type = models.CharField(max_length=20)
    post_mime_type = models.CharField(max_length=100)
    comment_count = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_posts'


class WpRevsliderCss(models.Model):
    handle = models.TextField()
    settings = models.TextField(blank=True, null=True)
    hover = models.TextField(blank=True, null=True)
    params = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_revslider_css'


class WpRevsliderLayerAnimations(models.Model):
    handle = models.TextField()
    params = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_revslider_layer_animations'


class WpRevsliderSettings(models.Model):
    general = models.TextField()
    params = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_revslider_settings'


class WpRevsliderSliders(models.Model):
    title = models.TextField()
    alias = models.TextField(blank=True, null=True)
    params = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_revslider_sliders'


class WpRevsliderSlides(models.Model):
    slider_id = models.IntegerField()
    slide_order = models.IntegerField()
    params = models.TextField()
    layers = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_revslider_slides'


class WpTermRelationships(models.Model):
    object_id = models.BigIntegerField(primary_key=True)
    term_taxonomy_id = models.BigIntegerField()
    term_order = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_term_relationships'
        unique_together = (('object_id', 'term_taxonomy_id'),)


class WpTermTaxonomy(models.Model):
    term_taxonomy_id = models.BigAutoField(primary_key=True)
    term_id = models.BigIntegerField()
    taxonomy = models.CharField(max_length=32)
    description = models.TextField()
    parent = models.BigIntegerField()
    count = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_term_taxonomy'
        unique_together = (('term_id', 'taxonomy'),)


class WpTermmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    term_id = models.BigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_termmeta'


class WpTerms(models.Model):
    term_id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    term_group = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_terms'


class WpUsermeta(models.Model):
    umeta_id = models.BigAutoField(primary_key=True)
    user_id = models.BigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_usermeta'


class WpUsers(models.Model):
    id = models.BigAutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    user_login = models.CharField(max_length=60)
    user_pass = models.CharField(max_length=255)
    user_nicename = models.CharField(max_length=50)
    user_email = models.CharField(max_length=100)
    user_url = models.CharField(max_length=100)
    user_registered = models.DateTimeField()
    user_activation_key = models.CharField(max_length=255)
    user_status = models.IntegerField()
    display_name = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'wp_users'


class WpWoocommerceApiKeys(models.Model):
    key_id = models.BigAutoField(primary_key=True)
    user_id = models.BigIntegerField()
    description = models.TextField(blank=True, null=True)
    permissions = models.CharField(max_length=10)
    consumer_key = models.CharField(max_length=64)
    consumer_secret = models.CharField(max_length=43)
    nonces = models.TextField(blank=True, null=True)
    truncated_key = models.CharField(max_length=7)
    last_access = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_api_keys'


class WpWoocommerceAttributeTaxonomies(models.Model):
    attribute_id = models.BigAutoField(primary_key=True)
    attribute_name = models.CharField(max_length=200)
    attribute_label = models.TextField(blank=True, null=True)
    attribute_type = models.CharField(max_length=200)
    attribute_orderby = models.CharField(max_length=200)
    attribute_public = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_attribute_taxonomies'


class WpWoocommerceDownloadableProductPermissions(models.Model):
    download_id = models.CharField(max_length=32)
    product_id = models.BigIntegerField()
    order_id = models.BigIntegerField()
    order_key = models.CharField(max_length=200)
    user_email = models.CharField(max_length=200)
    user_id = models.BigIntegerField(blank=True, null=True)
    downloads_remaining = models.CharField(max_length=9, blank=True, null=True)
    access_granted = models.DateTimeField()
    access_expires = models.DateTimeField(blank=True, null=True)
    download_count = models.BigIntegerField()
    permission_id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_downloadable_product_permissions'


class WpWoocommerceOrderItemmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    order_item_id = models.BigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_order_itemmeta'


class WpWoocommerceOrderItems(models.Model):
    order_item_id = models.BigAutoField(primary_key=True)
    order_item_name = models.TextField()
    order_item_type = models.CharField(max_length=200)
    order_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_order_items'


class WpWoocommercePaymentTokenmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    payment_token_id = models.BigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_payment_tokenmeta'


class WpWoocommercePaymentTokens(models.Model):
    token_id = models.BigAutoField(primary_key=True)
    gateway_id = models.CharField(max_length=255)
    token = models.TextField()
    user_id = models.BigIntegerField()
    type = models.CharField(max_length=255)
    is_default = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_payment_tokens'


class WpWoocommerceSessions(models.Model):
    session_id = models.BigAutoField(unique=True,primary_key=True)
    session_key = models.CharField(primary_key=True, max_length=32)
    session_value = models.TextField()
    session_expiry = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_sessions'


class WpWoocommerceShippingZoneLocations(models.Model):
    location_id = models.BigAutoField(primary_key=True)
    zone_id = models.BigIntegerField()
    location_code = models.CharField(max_length=255)
    location_type = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_shipping_zone_locations'


class WpWoocommerceShippingZoneMethods(models.Model):
    zone_id = models.BigIntegerField()
    instance_id = models.BigAutoField(primary_key=True)
    method_id = models.CharField(max_length=255)
    method_order = models.BigIntegerField()
    is_enabled = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_shipping_zone_methods'


class WpWoocommerceShippingZones(models.Model):
    zone_id = models.BigAutoField(primary_key=True)
    zone_name = models.CharField(max_length=255)
    zone_order = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_shipping_zones'


class WpWoocommerceTaxRateLocations(models.Model):
    location_id = models.BigAutoField(primary_key=True)
    location_code = models.CharField(max_length=255)
    tax_rate_id = models.BigIntegerField()
    location_type = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_tax_rate_locations'


class WpWoocommerceTaxRates(models.Model):
    tax_rate_id = models.BigAutoField(primary_key=True)
    tax_rate_country = models.CharField(max_length=200)
    tax_rate_state = models.CharField(max_length=200)
    tax_rate = models.CharField(max_length=200)
    tax_rate_name = models.CharField(max_length=200)
    tax_rate_priority = models.BigIntegerField()
    tax_rate_compound = models.IntegerField()
    tax_rate_shipping = models.IntegerField()
    tax_rate_order = models.BigIntegerField()
    tax_rate_class = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'wp_woocommerce_tax_rates'


class WpWpeditorSettings(models.Model):
    key = models.CharField(primary_key=True, max_length=50)
    value = models.TextField()

    class Meta:
        managed = False
        db_table = 'wp_wpeditor_settings'


class WpWpmmSubscribers(models.Model):
    id_subscriber = models.BigAutoField(primary_key=True)
    email = models.CharField(max_length=50)
    insert_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wp_wpmm_subscribers'
