from .util import WpUtil


wp = WpUtil()


#USER

user_count = wp.getUserCount()
users = wp.getUsers()
if user_count:
    user = wp.getUser(users[11].id)
    user_orders = wp.getUserOrders(user)
    user_subs = wp.getUserSubscriptions(user)


#ORDER

order_count = wp.getOrderCount()
orders = wp.getOrders()
if order_count:
    order = wp.getOrder(orders[10000].id)
    if user_orders:
        order_sub = wp.getOrderSubscription(user_orders[1])
        order_subs = wp.getOrderSubscriptions(user_orders[0])


#SUBSCRIPTION

sub_count = wp.getSubscriptionCount()
subs = wp.getSubscriptions()
if sub_count:
    sub = wp.getSubscription(subs[0].id)
    sub_orders = wp.getSubscriptionOrders(sub)
    sub_parent_order = wp.getSubscriptionOrder(sub)


#ITEM

item_count_all = wp.getItemCount(None)
items_all = wp.getItems(None)
if order_count:
    item_count = wp.getItemCount(orders[0])
    items = wp.getItems(orders[0])
    if item_count:
        item = wp.getItem(items[0].order_item_id)
        product = wp.getItemProduct(items[0])


#PRODUCT

product_count = wp.getProductCount()
products = wp.getProducts()
if product_count:
    product = wp.getProduct(products[0].id)


#COUPON

coupon_count = wp.getCouponCount()
coupons = wp.getCoupons()
if coupon_count:
    coupon = wp.getCoupon(coupons[0].id)
