from .models import WpPosts, WpPostmeta,\
                    WpUsers, WpUsermeta,\
                    WpWoocommerceOrderItems, WpWoocommerceOrderItemmeta,\
                    WpTermRelationships, WpTermTaxonomy,\
                    WpTerms, WpTermmeta


class WpUtil:

    #TYPES
    POST_TYPE_ORDER = "shop_order"
    POST_TYPE_PRODUCT = "product"
    POST_TYPE_PRODUCT_VARIATION = "product_variation"
    POST_TYPE_SUBSCRIPTION = "shop_subscription"
    POST_TYPE_COUPON = "shop_coupon"
    POST_TYPE_ATTACHMENT = "attachment"

    META_KEY_USER_ID = "_customer_user"
    META_KEY_SUBSCRIPTION_ID = "_subscription_renewal"


    DoesNotExist = WpPosts.DoesNotExist

    
    #USER

    def getUserCount(self):
        return self.getUsers().count()

    def getUsers(self):
        return WpUsers.objects.all()

    def getUserMeta(self,user):
        return WpUsermeta.objects.filter(user_id=user.id)

    def getUser(self,user_id):
        user = self.getUsers().get(id=user_id)
        user_meta = self.getUserMeta(user)
        return self.addMeta(user,user_meta)

    def getUserOrders(self,user):
        order_meta = self.getUserPostMeta(user)
        order_id_list = [om.post_id for om in order_meta]
        return self.getOrders().filter(id__in=order_id_list)

    def getUserSubscriptions(self,user):
        subscription_meta = self.getUserPostMeta(user)
        subscription_id_list = [om.post_id for om in subscription_meta]
        return self.getSubscriptions().filter(id__in=subscription_id_list)

    def getUserPostMeta(self,user):
        return WpPostmeta.objects.filter(meta_key=self.META_KEY_USER_ID,
                                         meta_value=user.id)

    def deleteUser(self,user):
        subscriptions = self.getUserSubscriptions(user)
        for subscription in subscriptions:
            self.deleteSubscription(subscription)

        orders = self.getUserOrders(user)
        for order in orders:
            self.deleteOrder(order)
        
        self.getUserMeta(user).delete()
        user.delete()


    #ORDER

    def getOrderCount(self):
        return self.getOrders().count()

    def getOrders(self):
        return WpPosts.objects.filter(post_type=self.POST_TYPE_ORDER)

    def getOrderMeta(self,order):
        return self.getPostMeta(order)

    def getOrder(self,order_id):
        order = self.getOrders().get(id=order_id)
        order_meta = self.getOrderMeta(order)
        return self.addMeta(order,order_meta)

    def getOrderSubscription(self,order):
        order = self.getOrder(order.id)
        
        try:
            subscription_id = order._subscription_renewal
        except AttributeError:
            return None

        return self.getSubscription(subscription_id)

    def getOrderSubscriptions(self,order):
        return self.getSubscriptions().filter(post_parent=order.id)

    def getOrderItems(self,order):
        return self.getItems().filter(order_id=order.id)

    def deleteOrder(self,order):
        order_subscriptions = self.getOrderSubscriptions(order)
        for subscription in order_subscriptions:
            self.deleteSubscription(subscription)

        order_items = self.getOrderItems(order)
        for order_item in order_items:
            self.deleteItem(order_item)

        self.getOrderMeta(order).delete()
        order.delete()


    #SUBSCRIPTION

    def getSubscriptionCount(self):
        return self.getSubscriptions().count()

    def getSubscriptions(self):
        return WpPosts.objects.filter(post_type=self.POST_TYPE_SUBSCRIPTION)

    def getSubscriptionMeta(self,subscription):
        return self.getPostMeta(subscription)

    def getSubscription(self,subscription_id):
        subscription = self.getSubscriptions().get(id=subscription_id)
        subscription_meta = self.getSubscriptionMeta(subscription)
        return self.addMeta(subscription,subscription_meta)

    def getSubscriptionOrder(self,subscription):
        order_id = subscription.post_parent
        return self.getOrder(order_id)

    def getSubscriptionOrders(self,subscription):
        order_meta = self.getSubscriptionPostMeta(subscription)
        order_id_list = [om.post_id for om in order_meta]
        return self.getOrders().filter(id__in=order_id_list)

    def getSubscriptionPostMeta(self,subscription):
        return WpPostmeta.objects.filter(meta_key=self.META_KEY_SUBSCRIPTION_ID,
                                         meta_value=subscription.id)

    def deleteSubscription(self,subscription):
        subscription_orders = self.getSubscriptionOrders(subscription)
        for order in subscription_orders:
            self.deleteOrder(order)
        self.getSubscriptionMeta(subscription).delete()
        subscription.delete()


    #ITEMS

    def getItemCount(self):
        return self.getItems().count()
    
    def getItems(self):
        return WpWoocommerceOrderItems.objects.all()

    def getItemMeta(self,item):
        return WpWoocommerceOrderItemmeta.objects.filter(order_item_id=item.order_item_id)

    def getItem(self,item_id):
        item = self.getItems().get(order_item_id=item_id)
        item_meta = self.getItemMeta(item)
        return self.addMeta(item,item_meta)

    def getItemProduct(self,item):
        try:
            product_id_meta = self.getItemMeta(item).get(meta_key="_product_id")
        except WpWoocommerceOrderItemmeta.DoesNotExist:
            return None

        product_id = product_id_meta.meta_value
        return self.getProduct(product_id)

    def deleteItem(self,item):
        self.getItemMeta(item).delete()
        item.delete()


    #PRODUCT

    def getProductCount(self):
        return self.getProducts().count()

    def getProducts(self):
        return WpPosts.objects.filter(post_type=self.POST_TYPE_PRODUCT)

    def getProductMeta(self,product):
        return self.getPostMeta(product)

    def getProduct(self,product_id):
        product = self.getProducts().get(id=product_id)
        product_meta = self.getProductMeta(product)
        product = self.addMeta(product,product_meta)
        product.terms = self.getProductTerms(product)
        product.attachments = self.getProductAttachments(product)
        return product

    def getProductVariations(self):
        return WpPosts.objects.filter(post_type=self.POST_TYPE_PRODUCT_VARIATION)

    def getProductVariationMeta(self,variation):
        return self.getPostMeta(variation)

    def getProductVariation(self,product_variation_id):
        product_variation = self.getProductVariations().get(id=product_variation_id)
        product_variation_meta = self.getProductVariationMeta(product_variation)
        return self.addMeta(product_variation,product_variation_meta)

    def getProductTerms(self,product):
        terms = []

        term_relationships = WpTermRelationships.objects.filter(object_id=product.id)
        for relationship in term_relationships:
            term_taxonomies = WpTermTaxonomy.objects.filter(term_taxonomy_id=relationship.term_taxonomy_id)
            for taxonomy in term_taxonomies:
                term = self.getTerm(taxonomy.term_id)
                terms.append(term)

        return terms

    def getProductAttachments(self,product):
        attachment_list = []

        attachments = self.getAttachments().filter(post_parent=product.id)
        for attachment in attachments:
            attachment = self.getAttachment(attachment.id)
            attachment_list.append(attachment)

        return attachment_list


    #TERMS

    def getTerms(self):
        return WpTerms.objects.all()

    def getTermMeta(self,term):
        return WpTermmeta.objects.filter(term_id=term.term_id)

    def getTerm(self,term_id):
        term = self.getTerms().get(term_id=term_id)
        term_meta = self.getTermMeta(term)
        return self.addMeta(term,term_meta)


    #ATTACHMENTS
    
    def getAttachments(self):
        return WpPosts.objects.filter(post_type=self.POST_TYPE_ATTACHMENT)

    def getAttachmentMeta(self,attachment):
        return self.getPostMeta(attachment)

    def getAttachment(self,attachment_id):
        attachment = self.getAttachments().get(id=attachment_id)
        attachment_meta = self.getAttachmentMeta(attachment)
        return self.addMeta(attachment,attachment_meta)


    #COUPON

    def getCouponCount(self):
        return self.getCoupons().count()

    def getCoupons(self):
        return WpPosts.objects.filter(post_type=self.POST_TYPE_COUPON)

    def getCouponMeta(self,coupon):
        return self.getPostMeta(coupon)

    def getCoupon(self,coupon_id):
        coupon = self.getCoupons().get(id=coupon_id)
        coupon_meta = self.getCouponMeta(coupon)
        return self.addMeta(coupon,coupon_meta)


    #COMMON

    def getPostMeta(self,post):
        return WpPostmeta.objects.filter(post_id=post.id)

    def addMeta(self,obj,meta_list):
        for meta in meta_list:
            meta_key = meta.meta_key
            meta_value = meta.meta_value
            setattr(obj,meta_key,meta_value)
        return obj
