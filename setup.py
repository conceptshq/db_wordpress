from setuptools import setup, find_packages

setup(name="db_wordpress",
      version="0.1",
      url="https://gitlab.com/conceptshq/db_wordpress",
      author="Concepts Collective, LLC",
      author_email="info@conceptscollective.com",
      description="Database interface for WordPress and WooCommerce",
      packages=find_packages(),
      include_package_data=True,
      )
